#!/usr/bin/env bash
set -euo pipefail

asdf plugin-add nodejs https://github.com/asdf-vm/asdf-nodejs.git
asdf install nodejs 14.5.0
asdf global 14.5.0


