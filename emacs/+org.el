


(after! org
  (setq org-agenda-clockreport-parameter-plist '(:link t :maxlevel 6 :stepskip0 t :fileskip0 t)))

(after! org
  (setq org-log-state-notes-into-drawer t)
  (defun my-org-config/setup-buffer-face ()
    (setq buffer-face-mode-face '(:family "iA Writer Mono S"))
    (buffer-face-mode)
    )
  (add-hook 'org-agenda-mode-hook 'my-org-config/setup-buffer-face)
  (defun my-org-config/after-org-mode-load ()
    (visual-line-mode)
    (vi-tilde-fringe-mode -1)

    (require 'org-indent)
    (org-indent-mode)
    (set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))

    (variable-pitch-mode 1)

    (require 'olivetti)
    (olivetti-mode)
    )

  (add-hook 'org-mode-hook 'my-org-config/after-org-mode-load)
  (defun my-org-config/after-org-archive ()
    (org-save-all-org-buffers))

  (add-hook 'org-archive-hook 'my-org-config/after-org-archive)
  (require 'org-checklist)

  (defun my-org-daily-agenda ()
    (interactive)
    (org-agenda nil "d")
    )
  (defun my-org-add-todo ()
    (interactive)
    (org-capture nil "t")
    )

  ;; (defun my-org-helm-find-file ()
  ;;   (interactive)
  ;;   (helm-browse-project-find-files "/Users/ash/org")
  ;;   )
  (defvar org-my-inbox-file "~/org/inbox.org")
  (defvar org-my-mobile-inbox-file "~/org/inbox_mobile.org")

  (setq org-default-notes-file org-my-inbox-file)

  (defvar org-my-general-files "~/org")

  (defvar org-my-projects-dir "~/org/projects")

  (add-to-list 'org-agenda-files org-my-general-files)
  (add-to-list 'org-agenda-files org-my-projects-dir)
  (setq org-refile-targets (quote (
                                   (org-agenda-files :maxlevel . 2)
                                   )))
  (setq org-reverse-note-order t)


  (org-super-agenda-mode)

  (setq org-super-agenda-header-map nil)

  (setq org-deadline-warning-days 7)
  (setq org-agenda-block-separator 9472)
  (setq org-agenda-skip-scheduled-if-done t)
  (setq org-agenda-start-on-weekday nil)



  (setq org-agenda-custom-commands '(
                                     ("a" "Dashboard"
                                      ((agenda "" (
                                                   (org-agenda-overriding-header "Week ahead")
                                                   (org-agenda-span 'week)
                                                   (org-agenda-scheduled-leaders '("  " "%2dx"))))
                                       (alltodo "" ((org-agenda-overriding-header "")
                                                (org-super-agenda-groups
                                                 '(
                                                   (:name "📋 Refile" :tag "inbox")
                                                   (:name "🧹 Ticklers" :and (:tag "tickler" :scheduled today))
                                                   (:name "❗ High Priority" :priority "A" :deadline today )
                                                   (:name "⏰ Quick Wins" :effort< "20")
                                                   )
                                                 )
                                                )
                                       )))
                                     ("h" "IOKI DASHBOARD"
                                      ((agenda "" (
                                                   (org-agenda-overriding-header "THIS WEEK")
                                                   (org-agenda-span 'week)
                                                   (org-agenda-scheduled-leaders '("   " "%2dx"))
                                                   ))
                                       (tags "+inbox"
                                             ((org-agenda-overriding-header "INBOX: Entries to refile")))
                                       (todo "VERIFY"
                                             ((org-agenda-overriding-header "FINAL VERIFICATION PENDING")))
                                       )
                                      )
                                     ("w" "WEEKLY REVIEW"
                                      (
                                       (todo "DONE"
                                             (
                                              (org-agenda-overriding-header "DONE!")
                                              ))
                                       (todo "CANCELLED"
                                             ((org-agenda-overriding-header "CANCELLED")))
                                       (todo "TODO"
                                             ((org-agenda-overriding-header "TODO Items (without time attached)")
                                              (org-agenda-skip-function '(org-agenda-skip-entry-if 'deadline 'scheduled 'timestamp))))
                                       (todo "WAIT"
                                             ((org-agenda-overriding-header "WAIT: Items on hold (without time attached)")
                                              (org-agenda-skip-function '(org-agenda-skip-entry-if 'deadline 'scheduled 'timestamp))))
                                       )
                                      )
                                     ("d" "DAILY"
                                      ((agenda "" ((org-agenda-span 'day)
                                                   (org-agenda-compact-blocks t)
                                                   ;; (org-agenda-deadline-leaders)
                                                   ;; (org-agenda-scheduled-leaders)
                                                   (org-agenda-prefix-format '(
                                                                               (agenda . "  %?-12t")
                                                                               ))
                                                   (org-super-agenda-groups
                                                    '(
                                                      (:name "⏰ Calendar" :time-grid t)
                                                      (:name "Optional" :priority "C" :order 90)

                                                      (:name "⚠ Overdue!" :deadline past)
                                                      (:name "⚠ Overdue!" :scheduled past)

                                                      ;; Discard full-day events from agenda
                                                      (:discard (:category "Cal"))

                                                      (:name "⭐ Next" :todo "NEXT")
                                                      (:name "⭐ Important" :priority "A")
                                                      (:name "📌 Routines" :category "Routines")

                                                      (:auto-category t)
                                                      ))
                                                   ))
                                       (alltodo "" ((org-agenda-overriding-header "")
                                                    (org-agenda-prefix-format '(
                                                                                (todo . "  ")
                                                                                ))
                                                    (org-super-agenda-groups
                                                     '(
                                                       (:name "🌆 End of day" :todo "ENDOFDAY")
                                                       (:name "Inbox" :tag "inbox")
                                                       (:name "Verify" :todo "VERIFY")
                                                       (:discard (:anything t))
                                                       )
                                                     ))))
                                      )
                                     ))


  (setq org-refile-use-outline-path 'file)
  (setq org-outline-path-complete-in-steps nil)
  (setq header-line-format " ")

  (setq org-ellipsis "  ")

  (setq org-hide-emphasis-markers t)

  (setq org-fontify-whole-heading-line t)

  (setq org-fontify-done-headline t)

  (setq org-fontify-quote-and-verse-blocks t)

  (setq org-superstar-headline-bullets-list '("⬢" "◆" "▲" "■"))

  (setq org-tags-column 0)

  (setq olivetti-body-width 81)

  (let* (
         (comment      "#6272a4")
         (warning      "#ffb86c")
         (rainbow-1    "#f8f8f2")
         (rainbow-2    "#8be9fd")
         (rainbow-3    "#bd93f9")
         (rainbow-4    "#ff79c6")
         (rainbow-5    "#ffb86c")
         (rainbow-6    "#50fa7b")
         (rainbow-7    "#f1fa8c")
         (rainbow-8    "#0189cc")
         (rainbow-9    "#ff5555")
         (rainbow-10   "#a0522d")

         (variable-pitch-font `(:family "iA Writer Quattro S" ))
         (fixed-pitch-font    `(:family "Fira Mono" ))
         (fixed-pitch-font-alt `(:family "iA Writer Mono S" )))

    (setq org-todo-keyword-faces (list
                                  `("TODO"
                                    ,@fixed-pitch-font
                                    :foreground ,comment
                                    :weight bold
                                    )
                                  `("NEXT"
                                    ,@fixed-pitch-font
                                    :foreground ,warning
                                    :weight bold)
                                  `("WAIT"
                                    ,@fixed-pitch-font
                                    :foreground ,rainbow-2
                                    :weight bold)
                                  `("VERIFY"
                                    ,@fixed-pitch-font
                                    :foreground ,rainbow-7
                                    :weight bold)
                                  `("ENDOFDAY"
                                    ,@fixed-pitch-font
                                    :foreground ,rainbow-3
                                    :weight bold)
                                  `("LOWPRIO"
                                    ,@fixed-pitch-font
                                    :foreground ,comment
                                    :weight bold)
                                  `("DONE"
                                    ,@fixed-pitch-font
                                    :foreground ,rainbow-6
                                    :weight bold)
                                  `("CANCELLED"
                                    ,@fixed-pitch-font
                                    :foreground ,rainbow-9
                                    :weight bold)
                                  ))
    )
  (setq org-src-fontify-natively t)
  (setq org-edit-src-content-indentation 0)
  (setq org-src-tab-acts-natively t)
  (setq org-src-preserve-indentation t)
  (setq org-confirm-babel-evaluate nil)

  (org-babel-do-load-languages 'org-babel-load-languages
                               '((sql . t)
                                 (shell . t)
                                 (dot . t)
                                 (emacs-lisp . t)
                                 (ruby . t)
                                 (js . t)
                                 (plantuml . t)))
  (setq org-plantuml-jar-path "/usr/local/Cellar/plantuml/1.2018.3/libexec/plantuml.jar")

  (setq org-html-htmlize-output-type 'css)

  (setq org-download-method 'attach)

  (require 'org-crypt)

  (setq org-tags-exclude-from-inheritance (quote ("crypt")))

  (setq org-crypt-key "5C0E1820")

  (org-crypt-use-before-save-magic)

  (setq org-crypt-disable-auto-save t)

  (setq org-tag-alist '(("crypt" . ?c)))

  (add-to-list 'org-capture-templates '("t" "Personal todo" entry
           (file+headline +org-capture-todo-file "Inbox")
           "* [ ] TODO %?\n%i\n%a" :prepend t))

  )
