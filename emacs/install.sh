link_file() {
	if [ -e "$2" ]; then
		if [ "$(readlink "$2")" = "$1" ]; then
			echo "skipped $1"
			return 0
		else
			mv "$2" "$2.backup"
			echo "moved $2 to $2.backup"
		fi
	fi
	ln -sf "$1" "$2"
	echo "linked $1 to $2"
}

	echo 'installing emacs dotfiles'
	find . -name '*.el' -maxdepth 3  |
		while read -r src; do
			dst="$HOME/.doom.d/$(basename "$src")"
			#link_file "$src" "$dst"
			link_file "$HOME/.dotfiles/emacs/$(basename $src)" "$dst"
		done
