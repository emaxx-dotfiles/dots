(provide '+local-config)
;;; +local-config.el ends here
(setq user-full-name "Ash McBride"
    user-mail-address "ash@ashmcbride.top" ; Use the email address linked to the GPG Key on this host
    calendar-latitude 53.464
    calendar-longitude -1.970
    calendar-location-name "Manchester, UK")

(defun +local-config/tcb ()
    "start TCB Standup"
    (interactive)
  (shell-command "open -a zoom.us 'https://us05web.zoom.us/j/8121400351?pwd=dm1nUW9XWkdISUtqdWxhN3U2WGNMZz09'")
  )



(setq mac-right-option-modifier 'nil)
(use-package! exec-path-from-shell)
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
