;;; config.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 John Doe
;;
;; Author: John Doe <https://github.com/ash1>
;; Maintainer: John Doe <john@doe.com>
;; Created: April 01, 2021
;; Modified: April 01, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/ash1/config
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:



(provide 'config)
;;; config.el ends here
;;; $DOOMDIR/config.el -*- lexical-binding: t! -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; refresh' after modifying this file!

;; load my personal private modules.
(load! "+local-config")
(load! "+org")


;; I use camelCase, global-subword-mode plus this config will ensure that the
;; 'Case' in camelCase is treated as a separate word when doing evil 'e' or 'b'
(global-subword-mode)
(setq require-final-newline t)

(define-category ?U "Uppercase")
(define-category ?u "Lowercase")
(modify-category-entry (cons ?A ?Z) ?U)
(modify-category-entry (cons ?a ?z) ?u)
(make-variable-buffer-local 'evil-cjk-word-separating-categories)
(add-hook 'subword-mode-hook
	  (lambda ()
            (if subword-mode
                (push '(?u . ?U) evil-cjk-word-separating-categories)
	      (setq evil-cjk-word-separating-categories
                    (default-value 'evil-cjk-word-separating-categories)))))


;; Set up projectile search path and ignored directories
(after! projectile
  (add-to-list 'projectile-globally-ignored-directories "node_modules")
  (add-to-list 'projectile-globally-ignored-directories "platforms")
    )

;; formatting with lsp is horrendously slow, so we disable.
(setq +format-with-lsp t)
;; add prettier and my standard linting style.
(use-package! prettier-js)

;; (after! prettier-js
;; (setq prettier-js-args '(
                         ;; "--use-tabs" "false"
                         ;; "--print-width" "100"
                         ;; "--tab-width" "2"
                         ;; "--single-quote" "false"
                         ;; "--trailing-comma" "es5"
                         ;; "--semi" "true"
                         ;; "--bracket-spacing" "true"
                         ;; "--html-whitespace-sensitivity" "css"
                         ;; "--arrow-parens" "always"
                         ;; ))

    ;; )

;; remove the delay on company's autocompletion
;; (after! company
;;   (setq company-idle-delay 0.05
;;         company-minimum-prefix-length 1
;;         ))






;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. These are the defaults.
(setq doom-theme 'doom-snazzy)

;; If you intend to use org, it is recommended you change this!
;; (setq org-directory "~/Dropbox/orgFiles")

;; If you want to change the style of line numbers, change this to `relative' or
;; `nil' to disable it:
(setq display-line-numbers-type 'relative)

(setq wakatime-api-key "ef8860e1-4ea7-4076-8992-bb3439410750")
(use-package! wakatime-mode)
;; (use-package! git-auto-commit-mode)
;; (use-package! org-wiki)
;; (use-package! import-cost)
;; (after! import-cost
;;   (setq import-cost-bundle-size-decoration 'gzipped)
;;   (add-hook 'js2-mode-hook #'import-cost-mode)
;;   (add-hook 'rjsx-mode-hook #'import-cost-mode)
;;   (add-hook 'typescript-mode-hook #'import-cost-mode))
;; (after! org-wiki
;;    (setq org-wiki-location "~/org/wiki" ))
;; (after! git-auto-commit-mode
;;   (git-auto-commit-mode 1)
;;   (setq gac-automatically-push-p t))

;; (global-wakatime-mode)
;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', where Emacs
;;   looks when you load packages with `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.
(setq racer-rust-src-path "/Users/ash/.rustup/toolchains/stable-x86_64-apple-darwin/lib/rustlib/src/rust/src")


(defun zz/tcb ()
    "start TCB Standup"
  (shell-command "xterm &")
  )


 ;; toggle ligatures in current buffer
(map! :leader
      (:prefix "t"
       :desc "ligatures (composition)" "L" #'auto-composition-mode))

;; Modeline adjustments
(setq doom-modeline-major-mode-icon t)

;; Treemacs
;; Fix icons and always follow opened file
(doom-themes-treemacs-config)
(after! treemacs
  (treemacs-load-theme "doom-colors")
  (setq treemacs-follow-mode t))


;; Set =C-middle click= to multi-cursor editing (evil)
(defun my/toggle-cursor-on-click (event)
  (interactive "e")
  (evil-mc-mode)
  (evil-mc-toggle-cursor-on-click event))
(map! "C-<down-mouse-2>" nil)
(map! "C-<mouse-2>" #'my/toggle-cursor-on-click)

;; Tree sitter global highlighting mode when available
(add-hook! 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)

;; lsp always show breadcrumb
(setq lsp-headerline-breadcrumb-enable t)


(after! lsp-mode
  (push "[/\\\\]_build\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]deps\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]cover\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]priv\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]\\.deliver\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]\\.elixir_ls\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]\\.build\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]node_modules\\'" lsp-file-watch-ignored-directories)
  (setq lsp-enable-folding nil))



(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'medium))


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(custom-safe-themes
   (quote
    ("425cf02839fa7c5ebd6cb11f8074f6b8463ae6ed3eeb4cf5a2b18ffc33383b0b" "bc836bf29eab22d7e5b4c142d201bcce351806b7c1f94955ccafab8ce5b20208" "6177ecbffb8f37756012c9ee9fd73fc043520836d254397566e37c6204118852" "e2acbf379aa541e07373395b977a99c878c30f20c3761aac23e9223345526bcc" "a92e9da0fab90cbec4af4a2035602208cebf3d071ea547157b2bfc5d9bd4d48d" "3d3807f1070bb91a68d6638a708ee09e63c0825ad21809c87138e676a60bda5d" "912cac216b96560654f4f15a3a4d8ba47d9c604cbc3b04801e465fb67a0234f0" "76bfa9318742342233d8b0b42e824130b3a50dcc732866ff8e47366aed69de11" "d71aabbbd692b54b6263bfe016607f93553ea214bc1435d17de98894a5c3a086" "3577ee091e1d318c49889574a31175970472f6f182a9789f1a3e9e4513641d86" "fe94e2e42ccaa9714dd0f83a5aa1efeef819e22c5774115a9984293af609fce7" default)))
 '(fci-rule-color "#505050")
 '(jdee-db-active-breakpoint-face-colors (cons "#1b1d1e" "#fc20bb"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1b1d1e" "#60aa00"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1b1d1e" "#505050"))
 '(objed-cursor-color "#d02b61")
 '(pdf-view-midnight-colors (cons "#dddddd" "#1b1d1e"))
 '(rustic-ansi-faces
   ["#1b1d1e" "#d02b61" "#60aa00" "#d08928" "#6c9ef8" "#b77fdb" "#00aa80" "#dddddd"])
 '(safe-local-variable-values (quote ((lexical-binding . t!))))
 '(vc-annotate-background "#1b1d1e")
 '(vc-annotate-color-map
   (list
    (cons 20 "#60aa00")
    (cons 40 "#859f0d")
    (cons 60 "#aa931a")
    (cons 80 "#d08928")
    (cons 100 "#d38732")
    (cons 120 "#d6863d")
    (cons 140 "#da8548")
    (cons 160 "#ce8379")
    (cons 180 "#c281aa")
    (cons 200 "#b77fdb")
    (cons 220 "#bf63b2")
    (cons 240 "#c74789")
    (cons 260 "#d02b61")
    (cons 280 "#b0345c")
    (cons 300 "#903d58")
    (cons 320 "#704654")
    (cons 340 "#505050")
    (cons 360 "#505050")))
 '(vc-annotate-very-old-color nil)
 '(wakatime-api-key "ef8860e1-4ea7-4076-8992-bb3439410750")
 '(wakatime-cli-path "/usr/local/bin/wakatime")
 '(wakatime-python-bin nil))
;; (load! "bindings")
